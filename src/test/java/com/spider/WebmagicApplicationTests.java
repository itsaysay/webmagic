package com.spider;

import com.spider.model.MoveDO;
import com.spider.service.SpiderService;
import com.spider.service.processor.Ygdy8PageProcessor;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import us.codecraft.webmagic.Spider;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class WebmagicApplicationTests {

	private static Spider spider = null;

	@Autowired
	private SpiderService spiderService;

	//@Test
	public void contextLoads() {


		try {
			spider =  Spider.create(new Ygdy8PageProcessor());
			spider.addUrl("http://s.dydytt.net/plus/search.php?kwtype=0&keyword="+ URLEncoder.encode("追龙","GB2312"))
					.addPipeline(new SpiderService())
					.thread(1)
					.run();

		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}

	}


}
