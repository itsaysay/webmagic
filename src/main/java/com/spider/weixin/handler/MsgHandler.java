package com.spider.weixin.handler;

import com.spider.model.MoveDO;
import com.spider.model.WaitListDO;
import com.spider.service.MoveService;
import com.spider.service.SpiderService;
import com.spider.service.WaitListService;
import com.spider.weixin.WeixinService;
import com.spider.weixin.builder.TextBuilder;
import com.xiaoleilu.hutool.util.ThreadUtil;
import me.chanjar.weixin.common.api.WxConsts;
import me.chanjar.weixin.common.session.WxSessionManager;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.bean.message.WxMpXmlMessage;
import me.chanjar.weixin.mp.bean.message.WxMpXmlOutMessage;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * 
 * @author Binary Wang
 *
 */
@Component
public class MsgHandler extends AbstractHandler {

    @Autowired
    private SpiderService spiderService;

    @Autowired
    private MoveService moveService;

    @Autowired
    private WaitListService waitListService;

    @Override
    public WxMpXmlOutMessage handle(WxMpXmlMessage wxMessage,
                                    Map<String, Object> context, WxMpService wxMpService,
                                    WxSessionManager sessionManager)    {

        WeixinService weixinService = (WeixinService) wxMpService;

        //回复内容
        String content = "";

        if (!wxMessage.getMsgType().equals(WxConsts.XML_MSG_EVENT)) {
            //TODO 可以选择将消息保存到本地
        }

        //当用户输入关键词如“你好”，“客服”等，并且有客服在线时，把消息转发给在线客服
        if (StringUtils.startsWithAny(wxMessage.getContent(), "你好", "客服")
            && weixinService.hasKefuOnline()) {
            return WxMpXmlOutMessage
                .TRANSFER_CUSTOMER_SERVICE().fromUser(wxMessage.getToUser())
                .toUser(wxMessage.getFromUser()).build();
        }else {
            //搜索电影
            String keyWord = wxMessage.getContent();
            List<MoveDO> list  = moveService.search(keyWord);
            if (list != null && list.size() > 0){
                content = "复制下载地址到迅雷即可:\n";
                for (MoveDO moveDO : list){
                    content += moveDO.getDownPath() + "\n";
                }

            }else {
                content = "请稍等...小影正在努力帮你搜索，找到发你消息哦";

                //去爬电影
                ThreadUtil.execute(new StartSpiderRun(keyWord,spiderService));

                //保存搜索，用于找到电影后发送消息给用户
                WaitListDO waitListDO = new WaitListDO();
                waitListDO.setKeyWord(keyWord);
                waitListDO.setOpenid(wxMessage.getFromUser());
                waitListService.save(waitListDO);
            }
        }



        //TODO 组装回复消息
        return new TextBuilder().build(content, wxMessage, weixinService);

    }

}

class StartSpiderRun implements  Runnable{

    private String keyword;

    private SpiderService spiderService;

    public StartSpiderRun(String keyword, SpiderService spiderService) {
        this.keyword = keyword;
        this.spiderService = spiderService;
    }

    @Override
    public void run() {
        spiderService.startSpider(keyword);
    }
}
