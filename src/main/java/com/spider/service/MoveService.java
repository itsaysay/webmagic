package com.spider.service;

import com.spider.dao.MoveRepository;
import com.spider.model.MoveDO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Administrator
 * @description
 * @date 2018-01-03
 */
@Service
public class MoveService {


    @Autowired
    private MoveRepository moveRepository;


    public List<MoveDO> search(String keyWord){
        List<MoveDO> list = moveRepository.findByTitleContaining(keyWord);
        updateMoveCount(list);
        return list;
    }


    @Async
    private void updateMoveCount(List<MoveDO> list){
        if (list != null){
            for (MoveDO moveDO : list){
                moveDO.setCount(moveDO.getCount() + 1);
                moveRepository.save(moveDO);
            }
        }
    }



}
