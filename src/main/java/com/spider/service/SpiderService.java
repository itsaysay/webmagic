package com.spider.service;

import com.spider.dao.SpiderRepository;
import com.spider.model.MoveDO;
import com.spider.model.WaitListDO;
import com.spider.service.processor.Ygdy8PageProcessor;
import com.spider.weixin.WeixinService;
import me.chanjar.weixin.common.exception.WxErrorException;
import me.chanjar.weixin.mp.bean.kefu.WxMpKefuMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import us.codecraft.webmagic.ResultItems;
import us.codecraft.webmagic.Spider;
import us.codecraft.webmagic.Task;
import us.codecraft.webmagic.pipeline.Pipeline;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;

/**
 * @author: jujun chen
 * @description: 爬虫服务类
 * @date: 2018/1/2
 */
@Service
public class SpiderService implements Pipeline {


    @Autowired
    private SpiderRepository spiderRepository;

    @Autowired
    private SpiderService spiderService;

    @Autowired
    private WeixinService weixinService;

    @Autowired
    private WaitListService waitListService;

    @Autowired
    private MoveService moveService;


    @Override
    public void process(ResultItems resultItems, Task task) {
        Object object =  resultItems.get("bean");
        if (object != null){
            try{
                spiderRepository.save((MoveDO)object);
            }catch (Exception e){
                e.printStackTrace();
            }
        }

    }


    public  void  startSpider(String keyWord){


        Spider spider = null;

        //阳光电影爬虫 http://www.ygdy8.com
        try {
            spider =  Spider.create(new Ygdy8PageProcessor());
            spider.addUrl("http://s.dydytt.net/plus/search.php?kwtype=0&keyword="+ URLEncoder.encode(keyWord,"GB2312"))
                    .addPipeline(spiderService)
                    .thread(2)
                    .run();

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        //抓取结束
        if (Spider.Status.Stopped.equals(spider.getStatus())){
            List<WaitListDO> waitList = waitListService.findByKeyWord(keyWord);
            List<MoveDO> result = moveService.search(keyWord);

            if (result.size() > 0){
                String content = "为您找到如下地址：\n";
                for (MoveDO moveDO:result){
                    content += moveDO.getDownPath() + "\n";
                }
                for (WaitListDO waitListDO: waitList){
                    WxMpKefuMessage message = WxMpKefuMessage
                            .TEXT()
                            .toUser(waitListDO.getOpenid())
                            .content(content)
                            .build();
                    try {
                        weixinService.getKefuService().sendKefuMessage(message);
                    } catch (WxErrorException e) {
                        e.printStackTrace();
                    }

                    //删除待发送消息列表
                    waitListService.delete(waitListDO.getId());
                }

            }

        }


    }


}
