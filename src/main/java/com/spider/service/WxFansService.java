package com.spider.service;

import com.spider.dao.WxFansRepository;
import com.spider.model.WxFansDO;
import com.xiaoleilu.hutool.util.BeanUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Administrator
 * @description
 * @date 2018-01-03
 */
@Service
public class WxFansService {


    @Autowired
    private WxFansRepository wxFansRepository;


    @Transactional
    public void save(WxFansDO wxFansDO){

        WxFansDO dbDO = wxFansRepository.findByOpenid(wxFansDO.getOpenid());

        if (dbDO == null){

            wxFansRepository.save(wxFansDO);
        }else {
            BeanUtil.copyProperties(wxFansDO,dbDO, BeanUtil.CopyOptions.create().setIgnoreNullValue(true));
        }

    }



}
