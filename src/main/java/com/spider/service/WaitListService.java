package com.spider.service;

import com.spider.dao.WaitListRepository;
import com.spider.model.WaitListDO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Administrator
 * @description
 * @date 2018-01-03
 */
@Service
public class WaitListService {


    @Autowired
    private WaitListRepository waitListRepository;


    public void delete(Long id){
        waitListRepository.delete(id);
    }


    /**
     * 新增
     * @param waitListDO
     */
    public void save(WaitListDO waitListDO){
        waitListRepository.save(waitListDO);
    }


    public List<WaitListDO> findByKeyWord(String keyword){
        List<WaitListDO> list = waitListRepository.findByKeyWord(keyword);
        return list;
    }


}
