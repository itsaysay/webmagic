package com.spider.task;

import com.spider.service.SpiderService;
import com.spider.service.processor.Ygdy8PageProcessor;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import us.codecraft.webmagic.Spider;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * @author: jujun chen
 * @description: 获取最新电影 http://www.ygdy8.com
 * @date: 2018/1/2
 */
@Log4j
@Component("ygdy8Task")
public class Ygdy8Task {


    @Autowired
    private SpiderService spiderService;

    //@Scheduled(cron = "2/30 * * ? * *")
    private void task(){
        Spider spider =  Spider.create(new Ygdy8PageProcessor());
        spider.addUrl("http://www.ygdy8.com/html/gndy/dyzz/list_23_1.html")
                .addPipeline(spiderService)
                .thread(1)
                .start();
    }



}


