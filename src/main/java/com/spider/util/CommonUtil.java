package com.spider.util;

import com.xiaoleilu.hutool.util.StrUtil;

/**
 * @Author: jujun chen
 * @Description:
 * @Date: 2018/1/2
 */
public class CommonUtil {


    /**
     * 如果object为null 或者为""返回空字符串
     * @param object
     * @return
     */
    public static String isBlank(Object object){
        if (object == null){
            return "";
        }
        if (StrUtil.isBlank(String.valueOf(object))){
            return "";
        }
        return StrUtil.trim(object.toString());
    }

}
