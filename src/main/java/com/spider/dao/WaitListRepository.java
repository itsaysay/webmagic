package com.spider.dao;

import com.spider.model.WaitListDO;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @author Administrator
 * @description
 * @date 2018-01-03
 */
public interface WaitListRepository extends JpaRepository<WaitListDO, Long> {

    List<WaitListDO> findByKeyWord(String keyword);
}
