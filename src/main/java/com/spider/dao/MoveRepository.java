package com.spider.dao;

import com.spider.model.MoveDO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * @author Administrator
 * @description
 * @date 2018-01-03
 */
public interface MoveRepository extends JpaRepository<MoveDO,Long> {

    @Query(value = "select move from MoveDO move where move.title like %:kwd%")
    List<MoveDO> findByTitleContaining(@Param("kwd") String keyWord);
}
