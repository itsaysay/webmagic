package com.spider.dao;

import com.spider.model.MoveDO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * @author: jujun chen
 * @description:
 * @date: 2018/1/2
 */
public interface SpiderRepository extends JpaRepository<MoveDO,Long>{

}
