package com.spider.dao;

import com.spider.model.WxFansDO;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Administrator
 * @description
 * @date 2018-01-03
 */
public interface WxFansRepository extends JpaRepository<WxFansDO,Long> {

    WxFansDO findByOpenid(String openid);
}
