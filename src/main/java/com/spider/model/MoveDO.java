package com.spider.model;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @author Administrator
 * @description 电影model
 * @date 2018-01-02
 */
@Data
@Entity
@Table(name = "move_list")
public class MoveDO implements Serializable{

    @Id
    @GeneratedValue
    private Long id;


    /**
     * 标题
     */
    @Column(name = "title")
    private String title;


    /**
     * 封面图片
     */
    @Column(name = "top_img_url")
    private String topImgUrl;


    /**
     * 年代
     */
    @Column(name = "time")
    private String time;


    /**
     * 产地
     */
    @Column(name = "place")
    private String place;


    /**
     * 类别
     */
    @Column(name = "category")
    private String category;


    /**
     * 导演
     */
    @Column(name = "director")
    private String director;


    /**
     * 主演
     */
    @Column(name = "star")
    private String star;


    /**
     * 状态
     */
    @Column(name = "status")
    private Integer status;


    /**
     * 简介
     */
    @Column(name = "description")
    private String description;


    /**
     * 下载地址
     */
    @Column(name = "down_path")
    private String downPath;


    /**
     * 搜索次数
     */
    @Column(name = "count")
    private Integer count;



    /**
     * 创建时间
     */
    @Column(name = "create_date")
    private Date createDate;


}
