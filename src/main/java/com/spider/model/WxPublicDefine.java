package com.spider.model;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author: jujunchen
 * @description: 接收微信公众号自定义配置
 * @date: 2017-11-08
 */
@Data
@Component("wxPublicDefine")
@ConfigurationProperties(prefix = "wx")
public class WxPublicDefine {

    private String appId;

    private String secret;

    private String token;

    private String aesKey;


}
