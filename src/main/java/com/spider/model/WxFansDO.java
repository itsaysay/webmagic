package com.spider.model;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


@Data
@Entity
@Table(name = "weixin_fans")
public class WxFansDO implements Serializable{

	/**
	 * 公众号粉丝管理
	 * 搜索参数默认赋值为空字符串，在sql判断的时候只需要判断不能空字符串即可，否则需要同时判断不为null
	 */

	@Id
	@GeneratedValue
	private Long id;

	@Column(name = "openid")
	private String openid;

	@Column(name = "nickname")
	private String nickname = "";

	@Column(name = "sex")
	private Integer sex;

	@Column(name = "city")
	private String city;

	@Column(name = "country")
	private String country;

	@Column(name = "province")
	private String province;

	@Column(name = "language")
	private String language;

	@Column(name = "headimgurl")
	private String headimgurl;

	@Column(name = "subscribe_time")
	@JSONField(format = "yyyy-MM-dd HH:mm:ss")
	private Date subscribeTime;

	@Column(name = "remark")
	private String remark;

	@Column(name = "groupid")
	private Integer groupid;

	@Column(name = "tagid_list")
	private String tagidList;

	@Column(name = "subscribe")
	private Boolean subscribe;


}
