package com.spider.model;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @author Administrator
 * @description 待发送消息队列model
 * @date 2018-01-02
 */
@Data
@Entity
@Table(name = "wait_list")
public class WaitListDO implements Serializable{

    @Id
    @GeneratedValue
    private Long id;


    @Column(name = "openid")
    private String openid;


    @Column(name = "key_word")
    private String keyWord;


}
